// Setup the dependecies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

//Allow access to routes defined within our application
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

const app = express();


// MongoDB Database Connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.ogakp.mongodb.net/Batch127_Capstone-2?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
);


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/users', userRoutes); //http://localhost:4001/users
app.use('/products', productRoutes); //http:localhost:4001/products
app.use('/orders', orderRoutes); //http:localhost:4001/orders


app.listen(process.env.PORT || 4001, ()=> {
	console.log(`API is now online on port ${ process.env.PORT || 4001}`)
})


/*

Specific Requirements
1. User Registration
2. User Authentication
3. Set User as Admin (Admin only)
4. Retrieve all active products
5. Retrieve a single product
6. Create Product (Admin only)
7. Update Product Information (Admin only)
8. Archieve Product (Admin only)
9. Non-admin User Checkout (Create Order)
10. Retrieve authenticated user's orders
11. Retrieve all orders (Admin only)

Data Model Requirements
1. User
	a. Email (String)
	b. Password (String)
	c. isAdmin (Boolean- defaults to false)

2. Product
	a. Name (String)
	b. Description (String)
	c. Price (Numbers)
	d. isAdmin (Boolean- defaults to true)
	e. createdOn (Date- defaults to current date of creation)
3. Order
	a. totalAmount
	b. purchasedOn (Date- defaults to current date of creation)
	c. Must be associated with:
		i. A user who owns the order
		ii. Products that belong to the order

*/