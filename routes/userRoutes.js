const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');


// Check if email exists
router.post('/checkEmail', (req, res) =>{
	userController.checkEmailExists(req.body).then(result => res.send(result));
})


// Check if username exists
router.post('/checkUsername', (req, res) =>{
	userController.checkUsernameExists(req.body).then(result => res.send(result));
})



//User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})


//User authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})


// Set User as Admin (ADMIN ONLY)
router.put('/setAsAdmin/:userId', auth.verify, (req, res) => {
	const userAuthority = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (userAuthority.isAdmin) {
		userController.setUserAsAdmin(req.params, req.body).then(result => res.send(result));
	}
	else {
		console.log("Invalid command! User is not an admin!");
		res.send(false);
	}
})


// Retrieve data of a user (ADMIN ONLY)
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userName: userData.userName}).then(result => res.send(result));
})


// User order cart
router.post('/cart', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
		// shippingAddress: req.body.shippingAddress,
		// quantity: req.body.quantity,
		// price: req.body.price
	}

	userController.cart(data).then(result => res.send(result));
})

// All user orders retrieval
router.get('/orders', auth.verify, (req, res) => {
	const userAuthority = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	let orderData = {
		orderList: req.body.orderList
	}

	if (userAuthority) {
		userController.getAllOrders(orderData).then(result => res.send(result));
	}
	else {
		console.log("Invalid command! User is not an admin!");
		res.send(false);
	}
})

// User order retrieval
router.get('/:userId/cart', auth.verify, (req, res) => {
	const userData = {
		userId: auth.decode(req.headers.authorization).id,
	}

	if (userData) {
		userController.getOrder(userData).then(result => res.send(result));
	}
	else {
		console.log("Invalid command! User not authenticated!");
		res.send(false);
	}
})



module.exports = router;

// Model -> Controllers -> Routes -> capstone2.js(main server)
