const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth');

//Route for creating a product
router.post('/', auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(result => res.send(result));
})


//Retrieve all products
router.get('/all', (req, res) => {
	productController.getAllProducts().then(result => res.send(result))
})


//Retrieve all ACTIVE products
router.get('/', (req, res) =>{
	productController.getAllActive().then(result => res.send(result))
})


//Retrieve SPECIFIC product
router.get("/:productId", (req,res)=>{
	productController.specificProduct(req.params)
	.then(result =>{
		res.send(result)
	})
})


//Update a product information
router.put('/:productId', auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	productController.updateProduct(req.params, req.body).then(result => res.send(result))
	}else{
		res.send(false)
		}

})


//Archive a product/soft delete a product
router.put('/archive/:productId', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		productController.archiveProduct(req.params).then(result => res.send(result))
	}else{
		res.send(false)
		}
})



module.exports = router;

// Model -> Controllers -> Routes -> capstone2.js(main server)
