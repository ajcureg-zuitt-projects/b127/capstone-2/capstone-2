const mongoose = require ('mongoose');

const productSchema = new mongoose.Schema({

	brandName: {
		type: String,
		required: [true, "Brand name is required"]
	},
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	category: {
		type: String,
		required: [true, "Category is required"]
	},
	description:{
		type: String,
		required: [true, "Product description is required"]
	},
	price:{
		type: Number,
		required: [true, "Product price is required"]
	},
	isAvailable: {
		type: Boolean,
		default: true,
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	consumers: [
		{
			userId: {
				type: String,
				required: [true, 'User ID is required']
			},
			// shippingAddress: {
			// 	type: String,
			// 	required: [true, 'Shipping Address is required']
			// },
			// quantity: {
   //              type: String,
   //              required: [true, 'Quantity is required']
   //          },
			orderedOn: {
				type: Date,
				default: new Date()
			},
		}
	]
});

module.exports = mongoose.model("Product", productSchema);
