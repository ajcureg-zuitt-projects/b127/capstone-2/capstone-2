// Create the Schema, model and export the file
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, 'First name is required']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required']
	},
	address: {
		type: String,
		required: [true, 'Address is required']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile No is required']
	},
	email: {
		type: String,
		required: [true, 'Email is required']
	},
	userName : {
		type: String,
		required: [true, 'Username is required']
	}, 
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			// shippingAddress: {
			// 	type: String,
			// 	required: [true, "Shipping Address is required"]
			// },
			// quantity: {
			// 	type: String,
			// 	default: "Pending",
			// 	required: [true, "Quantity is required"]
			// },
			// price: {
			// 	type: Number,
			// 	default: "Pending",
			// 	required: [true, "Price is required"]
			// },
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model('User', userSchema);

// Model -> Controllers -> Routes -> capstone2.js(main server)