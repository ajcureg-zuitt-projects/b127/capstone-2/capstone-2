const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find( { email: reqBody.email } ).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}


// Check if username exists
module.exports.checkUsernameExists = (reqBody) => {
	return User.find( { username: reqBody.username } ).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}


//User Registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		address: reqBody.address,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		userName: reqBody.userName,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


//User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne( {userName: reqBody.userName} ).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				return false;
			}
		}
	})
}


// Set User as Admin (ADMIN ONLY)
module.exports.setUserAsAdmin = (reqParams, reqBody) => {
	let updateUserStatus = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(reqParams.userId, updateUserStatus).then((user, error) => {
		if (error) {
			console.log("Error! User status update unsuccessful!");
			return false;
		} 
		else {
			console.log("Updated user as an admin!")
			return true;
		}
	})
}


// Retrieve data of a user (ADMIN ONLY)
module.exports.getProfile = (data) => {
	return User.find( data.userName ).then(result => {
		result.password = "";
		return result;
	})
}


// User cart
module.exports.cart = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push( { 
			productId: data.productId
			// shippingAddress: reqBody.shippingAddress,
			// quantity: reqBody.quantity,
			// price: reqBody.price
		} )

		return user.save().then((user, error) => {
			if (error) {
				console.log("Error!");
				return false;
			}
			else {
				console.log("Success!");
				return true;
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.consumers.push( { 
			userId: data.userId
			// shippingAddress: reqBody.shippingAddress,
			// quantity: reqBody.quantity 
		} );
		
		return product.save().then((product, error) => {
			if (error) {
				console.log("Error!");
				return false;
			}
			else {
				console.log("Success!");
				return true;
			}
		})
	})


	if (isUserUpdated && isProductUpdated) {
		console.log("User and Product Information have been successfully updated!")
		return true;
	}
	else {
		console.log("User and Product Information update failed!")
		return false;
	}
}

// All user orders retrieval
module.exports.getAllOrders = () => {
	return User.find( {isAdmin: false} ).then((result, error) => {
		if (error) {
			console.log("Error! Unable to retrieve all orders!")
			return false;
		}
		else {
			console.log("Retrieved all orders!")
			return result;
		}
	})
}

// User order retrieval
module.exports.getOrder = (data) => {
	return User.findById(data.userId).then((result, error) => {
		if (error) {
			console.log("Error! Retrieval of user's order is unsuccessful!")
			return false;
		}
		else {
			console.log("User's order has been retrieved successfully!")
			return result.orders;
		}
	})
}





// Model -> Controllers -> Routes -> capstone2.js(main server)