const Product = require('../models/Product');



//Creation of Product
module.exports.addProduct = (data) => {
	//User is an admin
	if(data.isAdmin){
		let newProduct = new Product({
			brandName: data.product.brandName,
			productName: data.product.productName,
			category: data.product.category,
			description: data.product.description,
			price: data.product.price
		})
		return newProduct.save().then((Product, error) => {
			if(error) {
				return false;
			}else {
				return true
			}
		})
	}else{
		return false;
	}
}


//Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


//Retrieve all ACTIVE products
module.exports.getAllActive= () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}


//Retrieve SPECIFIC product
module.exports.specificProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}


//Update a Product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else {
			return true;
		}
	})
}


//Archive a Product
module.exports.archiveProduct = (reqParams) => {
	let updatedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

// Model -> Controllers -> Routes -> capstone2.js(main server)